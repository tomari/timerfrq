	;; Timer frequency tester for DOS INT21h AH=2ah/2ch.
	;; 2012 H.Tomari. Public Domain
	;; compile>>> nasm -fbin -otimerfrq.com timerfrq.asm
	BITS 16
	org 100h
loopcnt equ 0ffffh 		; max. loop count
section .text
	cld
	mov bp,loopcnt
tprec:
	mov si,tbuf
	mov di,si
	call gettime
	lea si,[8+si]
	call gettime
	mov cx,4
	repe cmpsw
	jne found
	dec bp
	jnz tprec
	mov dx, msg_nf
	mov ah,9
	int 21h
	mov ax,4c01h		; exit status=01
	int 21h
found:
	mov si,tbuf
	mov di,dst1
	call dump4w
	mov di,dst2
	call dump4w
	mov dx, msg_f
	mov ah,9
	int 21h
	mov ax,4c00h		; exit status=00
	int 21h
dump4w: 			; Dump 4 words at SI to DI
	mov cx,4
dump1w:
	mov ax, word [si]
	call dump1b
	call dump1b
	loop dump1w
	ret
dump1b:				; Dump ah to DI
	mov bl,ah
	rol bl,4
	and bx,0fh
	mov bl, byte [bx+digits]
	mov [di], bl 		; fill char 1
	inc di
	mov bl,ah
	and bx,0fh
	mov bl, byte [bx+digits]
	mov [di], bl 		; fill char 2
	rol ax,8
	inc di
	inc si
	ret
gettime:			; get time. si=ptr to a 8-byte buffer
	mov ah,2ah		; get system date
	int 21h
	mov [0+si],cx
	mov [2+si],dx
	mov ah,2ch
	int 21h
	mov [4+si],cx
	mov [6+si],dx
	ret
section .data
msg_nf:
	db 'Clock did not change during this run.', 13, 10, '$'
msg_f:
	db 'Clock changed, dumping symptoms:', 13, 10
	db 'Year M D H M Scs', 13, 10
dst1:
	db 'XXXXXXXXXXXXXXXX', 13, 10
dst2:
	db 'XXXXXXXXXXXXXXXX', 13, 10, '$'
digits:
	db '0123456789ABCDEF'
section .bss
tbuf:
	resb 16
